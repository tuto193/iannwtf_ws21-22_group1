import numpy as np

# 1
µ = 0
ð = 1  # didn't find sigma on keyboard
dimensions = (5, 5)
n_d_array = np.random.normal(µ, ð, dimensions)

# 2
# NOTE: Since the text itself seems to have a typo, I will do what
# I interpret from it:
#   replace all entries of array with the squared original if
#   original > 0.09. Otherwise with 42
n_d_array[n_d_array <= 0.09] = 42
n_d_array[n_d_array != 42] *= n_d_array[n_d_array != 42]

# 3
print(n_d_array[:, 3])
