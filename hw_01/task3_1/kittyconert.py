from cat import Cat

if __name__ == "__main__":
    first_cat: Cat = Cat("Augustus")
    second_cat: Cat = Cat("Bob")
    first_cat.greet(second_cat.name)
