"""This is just our cat module."""


class Cat():
    """Class used to represent a cat and their interactions."""
    def __init__(self, name: str) -> None:
        """Name the cat (calls actual initilizing function)."""
        self.nitializer(name)

    def nitializer(self, name: str) -> None:
        """The actual constructor for this class."""
        self.name = name

    def greet(self, other_cat_name: str) -> None:
        """Print text of this cat greeting the other_cat_name."""
        print(
            f"Hello I am {self.name}! I see you are also a cool"
            f" fluffy kitty {other_cat_name}, let's purr at the human"
            " together, so that they shall give us food."
        )
