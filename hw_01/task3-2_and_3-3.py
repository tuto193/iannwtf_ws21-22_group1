###################################
#   3.2
###################################

squares: list = [x * x for x in range(101)]
# print(f"These are all the squares I have for you:\n{squares}")

###################################
#   3.3
###################################


class MyGenerator():
    """Generator pattern implementation in python."""
    def __init__(self) -> None:
        """Initialize the count to 1."""
        self.n = 1

    def __call__(self) -> None:
        """Print twice as many meows as in the last call."""
        # Print first the expected amount of meows, _stripping_
        # the final white space, since we don't want it.
        print(("Meow " * self.n).strip())
        # Double the amount of meows that will be printed next call.
        self.n *= 2


def my_yielding_generator() -> None:
    """This does the same as the Object above."""
    n = 1
    while True:
        print(("Meow " * n).strip())
        n *= 2
        # This is like "return", but instead of STOPPING the function
        # completely, it just SUSPENDS it, until it's called again.
        yield


my_gen = MyGenerator()
my_yge = my_yielding_generator()
for _i in range(10):
    print("This comes out of the object:")
    my_gen()
    print("And this from the function:")
    next(my_yge)
